import numpy as np

def takeVideo(camera, file_name = "Resources/live.h264"):
    camera.start_recording(file_name)
    camera.wait_recording(15)
    camera.stop_recording()
    return file_name