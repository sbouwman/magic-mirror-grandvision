# Importing external libraries
from PIL import Image, ExifTags
from google.cloud import storage
import os
import csv

# Importing internal libraries


# Credentials
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Resources/service_account.json"


def reorientImage(im):
    try:
        image_exif = im._getexif()
        image_orientation = image_exif[274]
        if image_orientation in (2, '2'):
            return im.transpose(Image.FLIP_LEFT_RIGHT)
        elif image_orientation in (3, '3'):
            return im.transpose(Image.ROTATE_180)
        elif image_orientation in (4, '4'):
            return im.transpose(Image.FLIP_TOP_BOTTOM)
        elif image_orientation in (5, '5'):
            return im.transpose(Image.ROTATE_90).transpose(Image.FLIP_TOP_BOTTOM)
        elif image_orientation in (6, '6'):
            return im.transpose(Image.ROTATE_270)
        elif image_orientation in (7, '7'):
            return im.transpose(Image.ROTATE_270).transpose(Image.FLIP_TOP_BOTTOM)
        elif image_orientation in (8, '8'):
            return im.transpose(Image.ROTATE_90)
        else:
            return im
    except (KeyError, AttributeError, TypeError, IndexError):
        return im


def upload_cloud_storage(file_name=None, file_name_storage=None):
    """
    This function takes a file and uploads it to google cloud storage
    :param filename: any file
    :return:
    """
    storage_client = storage.Client()
    bucket = storage_client.get_bucket("images_to_train_with_v2")
    blob = bucket.blob(file_name_storage)
    blob.upload_from_filename(file_name)
    bucket = "gs://images_to_train_with_v2/" + file_name_storage

    return bucket


def fileAdress_to_csv(bucket=None, model=None, category=None):
    """
    This function taskes in a bucket and writes it into a csv file. If the csv file does not exist, it creates it.
    :param bucket: name of the address of the file in gcp gs://..........
    :param folder: the local folder where you want to save the csv file
    """
    if os.path.isfile("Resources/" + model + "_locations.csv"):
        with open("Resources/" + model + "_locations.csv", "r") as in_file:
            reader = list(csv.reader(in_file))
            row = [bucket, model, category]
            reader.insert(1, row)

        with open("Resources/" + model + "_locations.csv", "w") as outfile:
            writer = csv.writer(outfile)
            for line in reader:
                writer.writerow(line)
    else:
        with open("Resources/" + model + "_locations.csv", "w") as outfile:
            writer = csv.writer(outfile)
            row = [bucket, model, category]
            writer.writerow(row)
