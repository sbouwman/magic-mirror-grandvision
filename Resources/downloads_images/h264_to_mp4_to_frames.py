import os
import cv2
import time



#list dir
dirs = os.listdir()
for dir in dirs:
    print(dir)
    if ".DS_Store" in dir or ".py" in dir or ".xml" in dir:
        pass
    else:
        files = os.listdir(dir)
        for file in files:
            if ".DS_Store" in file:
                pass
            else:
                print(file)
                vidcap = cv2.VideoCapture(f"{dir}/{file}")
                success, image = vidcap.read()
                count = 0
                while success:
                    cv2.imwrite(f"{dir}/{file}%d.jpg" % count, image)  # save frame as JPEG file
                    success, image = vidcap.read()
                    print('Read a new frame: ', success)
                    count += 1


