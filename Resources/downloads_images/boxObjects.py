# Importing external libraries
import os, sys, csv
import cv2
import numpy as np
from google.cloud import vision
from google.cloud.vision import types
from PIL import Image

# Importing internal libraries
sys.path.append('/Users/silbouwman/PycharmProjects/GrandVision_PoC_smartmirror')
from Resources.help_scripts.functions import upload_cloud_storage

os.chdir('/Users/silbouwman/PycharmProjects/GrandVision_PoC_smartmirror/Resources/downloads_images')

os.environ[
    "GOOGLE_APPLICATION_CREDENTIALS"] = "/Users/silbouwman/PycharmProjects/GrandVision_PoC_smartmirror/Resources/service_account.json"

# Function for detecting faces
faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')


def recognizeFace(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )


# Anootating the image
dirs = os.listdir()
for dir in dirs:
    if ".DS_Store" in dir or ".py" in dir or ".xml" in dir:
        pass
    else:
        print(dir)

        #Remove any old csv files
        filelist = [f for f in os.listdir(dir) if f.endswith(".csv")]
        for f in filelist:
            os.remove(os.path.join(dir, f))

        #List the files in the dir
        files = os.listdir(dir)
        path_csv_file = dir + "/locations.csv"
        for file in files:
            if ".jpg" in file:
                # Check if there is a face in the image
                image_asarray = cv2.imread(f"{dir}/{file}")
                faces = recognizeFace(image_asarray)
                if 0 < len(faces) < 2:
                    print("--------------------/n-----------------------")
                    print("face detected")
                    height, width, channels = image_asarray.shape
                    for (x, y, w, h) in faces:
                        result = {
                            "object": "no_glasses",
                            "xmin": x / width,  # Getting the two diagonals
                            "ymin": y / height,
                            "xmax": (x + w) / width,
                            "ymax": (y + h) / height
                        }
                        print(f"face: {result}")
                        print("--------------------/n-----------------------")


                # Check if there are glasses in the image, in that case overwrite the results of the face detection
                with open(f"{dir}/{file}", "rb") as image:
                    image_bytes = image.read()

                client = vision.ImageAnnotatorClient()
                image = types.Image(content=image_bytes)

                # Performs label detection on the image file to see if there are glasses in the image
                response = client.object_localization(image=image)
                objects = response.localized_object_annotations
                for object in objects:
                    if object.name.lower() == 'glasses':
                        print("--------------------/n-----------------------")
                        print("glasses detected")
                        result = {
                            "object": "glasses",
                            "xmin": object.bounding_poly.normalized_vertices[0].x,  # Getting the two diagonals
                            "ymin": object.bounding_poly.normalized_vertices[0].y,
                            "xmax": object.bounding_poly.normalized_vertices[2].x,
                            "ymax": object.bounding_poly.normalized_vertices[2].y
                        }
                        print(f"glasses: {result}")
                        print("--------------------/n-----------------------")


                # Upload image to the cloud and get the bucket
                bucket = upload_cloud_storage(f"{dir}/{file}", f"{dir}/{file}")

                # Setting the location of the image in a csv
                if result["object"] == "glasses":
                    row = ["UNASSIGNED", bucket, dir, result["xmin"], result["ymin"], "", "", result["xmax"],
                           result["ymax"], "", ""]
                elif result["object"] == "no_glasses":
                    row = ["UNASSIGNED", bucket, "no_glasses", result["xmin"], result["ymin"], "", "", result["xmax"],
                           result["ymax"], "", ""]

                # updating csv
                if os.path.isfile(path_csv_file):
                    with open(path_csv_file, "r") as in_file:
                        reader = list(csv.reader(in_file))
                        reader.insert(1, row)

                    with open(path_csv_file, "w") as outfile:
                        writer = csv.writer(outfile)
                        for line in reader:
                            writer.writerow(line)
                else:
                    with open(path_csv_file, "w") as outfile:
                        writer = csv.writer(outfile)
                        writer.writerow(row)
            else:
                pass

            # Uploading the csv file to cloud storage
            if os.path.isfile(path_csv_file):
                bucket = upload_cloud_storage(path_csv_file, path_csv_file)
                print(bucket)

#MAKE PICTURES WITH THE SNAPSHOT TOOL
#COMMANDS:
    # imagesnap -l
    # imagesnap -d 'C922 Pro Stream Webcam' -t 0.2

#DOWNLOAD PICTURES FROM GOOGLE CLOUD
#COMMANDS
    # Go to folder where you want to download
    # gsutil cp gs://grandvision-poc-smartmirror-vcm/Resources/3_ISKM05_HN /*.jpg .

# GLASSES MODELS
    # GOOD: ["CLDF06_HB",  "FAHF10_RX", "FAHM09_BB", "DBHF01_RR", "FAHM18_HG"]
    # NOT SO GOOD: ["ISKM05_HN", "CLEM06_GN", "SNHM02_DD"]
    # NOT: ["FAJM08_GG", "SNGM06_BB", "DBCM22_EE", "FACF42_CS", "CLDF06_NP", "no_glasses]

























# GLASSES MODELS
    # ["CLDF06_HB", "CLEM06_GN", "DBHF01_RR", "FAHF10_RX", "FAHM09_BB", "FAJM08_GG", "ISKM05_HN", "SNGM06_BB", "SNHM02_DD", THE_JOHNY, THE_NEFF, no_glasses]


















