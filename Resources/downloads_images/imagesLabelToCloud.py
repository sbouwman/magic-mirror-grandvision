# Importing external libraries
from google.cloud import storage
import os, csv
from datetime import datetime

os.environ[
    "GOOGLE_APPLICATION_CREDENTIALS"] = "/Users/silbouwman/PycharmProjects/GrandVision_PoC_smartmirror/service_account.json"


#Models and categories
#categories_glasses = ["Dark_Rectangle", "Metal_Round", "Plastic_Round", "Plastic_Square_Cateye"]
#models_glasses = ["CLDF06_HB", "CLEM06_GN", "FAHF10_RX", "FAHM09_BB", "FAJM08_GG", "ISKM05_HN", "SNGM06_BB", "SNHM02_DD", "DBHF01_RR", "FAHM18_HG"]

#Function for uploading to cloud
def upload_cloud_storage(file_name=None, file_name_storage=None):
    """
    This function takes a file and uploads it to google cloud storage
    :param filename: any file
    :return:
    """
    storage_client = storage.Client()
    bucket = storage_client.get_bucket("grandvision-poc-smartmirror-vcm")
    blob = bucket.blob(file_name_storage)
    blob.upload_from_filename(file_name)
    bucket = "gs://grandvision-poc-smartmirror-vcm/" + file_name_storage

    return bucket

#Remove any old csv files
filelist = [f for f in os.listdir() if f.endswith(".csv")]
for f in filelist:
    os.remove(os.path.join(f))

#CSV file
path_csv_file = f"locations{str(datetime.now())}.csv"
print(path_csv_file)

# Annotating the image
dirs = os.listdir()
for dir in dirs:
    if ".DS_Store" in dir or ".py" in dir or ".xml" in dir or ".csv" in dir or ".txt" in dir:
        continue

    print(dir)

    #List the files in the dir
    files = os.listdir(dir)

    counter = 0
    for file in files:
        counter += 1
        if counter % 10 == 0:
            print(f"file {counter} out of {len(files)}")

        if ".jpg" in file:

            # Upload image to the cloud and get the bucket
            bucket = upload_cloud_storage(f"{dir}/{file}", f"poc_10_models/{file}")

            #Getting the category
            if dir in ["FAHM09_BB", "SNGM06_BB"]:
                category = "Dark_Rectangle"
            elif dir in ["SNHM02_DD", "CLEM06_GN"]:
                category = "Metal_Round"
            elif dir in ["ISKM05_HN", "FAJM08_GG"]:
                category = "Plastic_Round"
            elif dir in ["CLDF06_HB", "FAHF10_RX"]:
                category = "Plastic_Square_Cateye"
            else:
                category = False

            # Setting the location of the image in a csv
            if category:
                row = [bucket, dir, category]
            else:
                row = [bucket, dir]

            # updating csv
            if os.path.isfile(path_csv_file):
                with open(path_csv_file, "r") as in_file:
                    reader = list(csv.reader(in_file))
                    reader.insert(1, row)

                with open(path_csv_file, "w") as outfile:
                    writer = csv.writer(outfile)
                    for line in reader:
                        writer.writerow(line)
            else:
                with open(path_csv_file, "w") as outfile:
                    writer = csv.writer(outfile)
                    writer.writerow(row)
        else:
            pass

        # Uploading the csv file to cloud storage
if os.path.isfile(path_csv_file):
    bucket = upload_cloud_storage(path_csv_file, f"poc_10_models/{path_csv_file}")
    print(bucket)