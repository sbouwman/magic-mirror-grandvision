#Importing external libraries
import sys, os, cv2

# Importing internal libraries
sys.path.append('/Users/silbouwman/PycharmProjects/GrandVision_PoC_smartmirror')
from Models.util import cropImage
os.chdir('/Users/silbouwman/PycharmProjects/GrandVision_PoC_smartmirror/Resources/downloads_images')

# Function for detecting faces
faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

def recognizeFace(orig_image):
    width = 640
    height = 360
    image = cv2.resize(orig_image, dsize=(width, height), interpolation=cv2.INTER_CUBIC)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(60, 60),
        flags = cv2.CASCADE_SCALE_IMAGE
    )
    return (faces * int(orig_image.shape[0] / height))


#TODO: FIRST MOVE ALL ORIGINAL FILES TO THE ORIGINAL FOLDER

dirs = os.listdir()
for dir in dirs:
    if ".DS_Store" in dir or ".py" in dir or ".xml" in dir or ".csv" in dir or ".txt" in dir:
        pass
    else:
        print(dir)

        # List the files in the dir
        files = os.listdir(dir)
        print(files)
        for file in files:
            if ".jpg" in file:
                # Check if there is a face in the image
                image_asarray = cv2.imread(f"{dir}/{file}")
                faces = recognizeFace(image_asarray)
                if len(faces) > 0:
                    print("face detected")
                    crop_image = cropImage(faces=faces, image=image_asarray)
                    os.remove(f"{dir}/{file}")
                    cv2.imwrite(f"{dir}/{file}", crop_image)
                    print("--------------------/n-----------------------")
                else:
                    print("no face detected")
                    os.remove(f"{dir}/{file}")
                    print("--------------------/n-----------------------")