# External libraries
from flask import Flask, send_file, make_response, jsonify
from flask_cors import CORS
import os
import json
import time
import io
import logging

# Internal libraries
from Models.classify_image import classifyImage
from Models.util import recognizeFace, drawFaces, cropImage
from Models.classify_if_glasses import hasGlasses
from Models.classify_image_local import classifyImageLocal
from classes.camera_local import Camera

# Project variables
app = Flask(__name__)
CORS(app)
environment = os.getenv("ENV") # Must be development or production, default to development
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./service_account.json"

#initiate camera
camera = Camera()

@app.route("/face", methods=["GET"])
def hasFace():
    image = camera.takePicture()
    faces = recognizeFace(image)
    print(faces)

    if len(faces) > 0:
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}

    return json.dumps({'success': False}), 200, {'ContentType': 'application/json'}


@app.route("/info", methods=["GET"])
def get_info():
    image = camera.takePicture()
    faces = recognizeFace(image)
    if len(faces) < 1:
        return json.dumps({'info': "no face detected"}), 200, {'ContentType': 'application/json'}
    else:
        image_cropped = cropImage(faces=faces, image=image)
        image_cropped_jpg = camera.toJpg(image_cropped)
        response = classifyImage(image_cropped_jpg)
        if response == {}:
            return json.dumps({"info": ""}), 200, {'ContentType': 'application/json'}
        else:
            return json.dumps({"info": response}), 200, {'ContentType': 'application/json'}



@app.route("/info_local", methods=["GET"])
def glasses_local():
    return json.dumps(classifyImageLocal()), 200, {'ContentType': 'application/json'}


@app.route("/pic", methods=["GET"])
def takePic():
    print("incoming request")
    image = camera.takePicture()
    faces = recognizeFace(image)
    if len(faces) == 0:
        return '', 404
    
    image_cropped = cropImage(faces=faces, image=image)

    bytes_file = camera.toJpg(image_cropped)
    response = make_response()
    response.mimetype = 'image.jpeg'
    response.data = bytes_file.read()
    return response
    # return send_file(bytes_file, as_attachment=True, mimetype="image/jpeg"), 200


@app.route("/ping", methods=["GET"])
def ping():
    return 'pong', 200


@app.route("/glasses", methods=["GET"])
def glasses():
    bytes_file = camera.getJpg()
    return json.dumps({"success": hasGlasses(bytes_file)}), 200, {'ContentType': 'application/json'}



if __name__ == "__main__":
    app.run(port=5000, debug=True, host="0.0.0.0", threaded=True)

