import time
import cv2

from classes.camera_base import CameraBase

video_capture = cv2.VideoCapture(0) #Change if you want to use the webcam of your laptop

class Camera(CameraBase):
    #TODO SET RESOLUTIONS
    def __init__(self):
        super().__init__()
    
    
    def takePicture(self):
        ret, frame = video_capture.read()
        return cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
