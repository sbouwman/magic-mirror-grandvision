import numpy as np
from PIL import Image
from io import BytesIO

class CameraBase:

    def __init__(self):
        print('Init base camera')

    def takePicture(self):
        output = np.empty((720, 1280, 3), dtype=np.uint8)
        return output

    def toImage(self, arr):
        return Image.fromarray(arr, mode="RGB")

    def toJpg(self, arr):
        image = self.toImage(arr)
        temp = BytesIO()
        image.save(temp, format="JPEG")
        temp.seek(0)
        return temp

    def getImage(self):
        arr = self.takePicture()
        return self.toImage(arr)

    def getJpg(self):
        arr = self.takePicture()
        return self.toJpg(arr)

