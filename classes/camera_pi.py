import time
import numpy as np
from picamera import PiCamera

from classes.camera_base import CameraBase

class Camera(CameraBase):
    def __init__(self):
        print('init pi camera')
        super().__init__()
    
    def takePicture(self):
        print('take pi camera picture')
        output = np.empty((720, 1280, 3), dtype=np.uint8)

        with PiCamera() as camera:
            camera.resolution = (1280, 720)
            camera.capture(output, 'rgb')
            camera.close()
        
        return output
