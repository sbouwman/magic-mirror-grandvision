# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

def hasGlasses(image_file):
    # Instantiates a client
    image_bytes = image_file.read()
    client = vision.ImageAnnotatorClient()
    image = types.Image(content=image_bytes)

    # Performs label detection on the image file
    response = client.object_localization(image=image)
    objects = response.localized_object_annotations
    for object in objects:
        if object.name.lower() == 'glasses':
            return True
    
    return False

def boxGlasses(image_file):
    # Instantiates a client
    image_bytes = image_file.read()
    client = vision.ImageAnnotatorClient()
    image = types.Image(content=image_bytes)

    # Performs label detection on the image file
    response = client.object_localization(image=image)
    objects = response.localized_object_annotations
    for object in objects:
        if object.name.lower() == 'glasses':
            print("glasses detected")
            print(object)
            print("--------------------/n-----------------------")
            result = {
                "object": "glasses",
                "xmin": object.bounding_poly.normalized_vertices[0].x, #Getting the two diagonals
                "ymin": object.bounding_poly.normalized_vertices[0].y,
                "xmax": object.bounding_poly.normalized_vertices[2].x,
                "ymax": object.bounding_poly.normalized_vertices[2].y
            }
            return result
        else:
            pass

    return None