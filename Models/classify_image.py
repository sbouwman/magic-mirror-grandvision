from google.cloud import automl_v1beta1 as automl
import os
import time

project_id = 'grandvision-poc-smartmirror'
compute_region = 'us-central1'
model_id = 'ICN2679893151271447948'
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./service_account.json"

categories_glasses = ["Dark_Rectangle", "Plastic_Square_Cateye"] #["Dark_Rectangle", "Metal_Round", "Plastic_Round", "Plastic_Square_Cateye"]
models_glasses = ["CLDF06_HB", "FAHF10_RX", "FAHM09_BB"] #["CLDF06_HB", "CLEM06_GN", "FAHF10_RX", "FAHM09_BB", "FAJM08_GG", "ISKM05_HN", "SNGM06_BB", "SNHM02_DD"]

# One-time setup
automl_client = automl.AutoMlClient()
# Get the full path of the model.
model_full_id = automl_client.model_path(project_id, compute_region, model_id)
prediction_client = automl.PredictionServiceClient()

def classifyImage(file):
    image_bytes = file.read()
    payload = {'image': {'image_bytes': image_bytes}}  # Put in image_bytes if pathname

    start_time = time.time()
    result = prediction_client.predict(model_full_id, payload)
    print(time.time() - start_time)
    print(result)
    response = {}
    category_counter = 0
    model_counter = 0

    for i in range(0, len(result.payload)): #This assumes that google responds with the labels ordered on coinfidence levels
        label = result.payload[i].display_name
        if label in categories_glasses and category_counter < 1:
            category_counter =+ 1
            coinfidence = result.payload[i].classification.score
            response["category"] = {"label": label, "confidence": coinfidence}
        elif label in models_glasses and model_counter < 1:
            model_counter =+ 1
            coinfidence = result.payload[i].classification.score
            response["model"] = {"label": label, "confidence": coinfidence}
        else:
            pass



    return response
