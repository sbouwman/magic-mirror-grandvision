import cv2

faceCascade = cv2.CascadeClassifier('Resources/haarcascade_frontalface_default.xml')

def recognizeFace(orig_image):
    width = 640
    height = 360
    image = cv2.resize(orig_image, dsize=(width, height), interpolation=cv2.INTER_CUBIC)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(60, 60),
        flags = cv2.CASCADE_SCALE_IMAGE
    )
    return (faces * int(orig_image.shape[0] / height))

def drawFaces(faces, image):
    for (x,y,w,h) in faces:
        cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)
    return image

def cropImage(faces, image):
    cropped_images = []
    sizes = []
    for (x, y, w, h) in faces:
        crop_img = image[y:y+h, x:x+w]
        cropped_images.append(crop_img)
        sizes.append(w*h)

    #Only getting the biggest box, since sometimes it has some noise
    index = sizes.index(max(sizes))
    crop_img = cropped_images[index]
    return crop_img

