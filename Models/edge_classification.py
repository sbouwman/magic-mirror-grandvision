import tensorflow as tf
import numpy as np
from PIL import Image

MODEL_PATH = 'Resources/models_edge_ICN6310236091011295803_2019-09-18_07-40-40-659_tflite_model.tflite'
LABEL_PATH = 'Resources/models_edge_ICN6310236091011295803_2019-09-18_07-40-40-659_tflite_dict.txt'

CATEGORIES = ["Dark_Rectangle", "Plastic_Square_Cateye"] #["Dark_Rectangle", "Metal_Round", "Plastic_Round", "Plastic_Square_Cateye"]
MODELS = ["CLDF06_HB", "FAHF10_RX", "FAHM09_BB"] #["CLDF06_HB", "CLEM06_GN", "FAHF10_RX", "FAHM09_BB", "FAJM08_GG", "ISKM05_HN", "SNGM06_BB", "SNHM02_DD"]

def load_labels(filename):
  with open(filename, 'r') as f:
    return [line.strip() for line in f.readlines()]

def edge_classification(img):
# Init TFlite interpreter
    interpreter = tf.lite.Interpreter(model_path=MODEL_PATH)
    interpreter.allocate_tensors()

    # Get input and output tensors.
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # Get input dimensions
    height = input_details[0]['shape'][1]
    width = input_details[0]['shape'][2]

    # Resize image to input dimensions
    print('resizing from {}/{} to {}/{}'.format(img.size[0], img.size[1], width, height))
    resized_img = img.resize((width, height))

    # Predict image
    input_data = np.expand_dims(resized_img, axis=0)
    interpreter.set_tensor(input_details[0]['index'], input_data)
    interpreter.invoke()

    # Get output
    output_data = interpreter.get_tensor(output_details[0]['index'])
    results = np.squeeze(output_data)

    # Format output
    top_k = results.argsort()[-5:][::-1]
    labels = load_labels(LABEL_PATH)
    response = {}
    for i in top_k:
        print('{:08.6f}: {}'.format(float(results[i] / 255.0), labels[i]))
        if labels[i] in MODELS and not 'model' in response:
            response['model'] = {'label': labels[i], "confidence":  float(results[i] / 255.0)}
        elif labels[i] in CATEGORIES and not 'category' in response:
            response['category'] = {'label': labels[i], "confidence":  float(results[i] / 255.0)}

    # Check for a threshold of coinfidence
    confidence_threshold = 0.5
    if response["model"]["confidence"] < confidence_threshold:
        response = {}

    return response