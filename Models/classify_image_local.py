import tensorflow as tf
import numpy as np
from PIL import Image

from classes.camera_local import Camera
from Models.util import recognizeFace
from Models.util import cropImage
from Models.edge_classification import edge_classification

camera = Camera()

def classifyImageLocal():
    response = {}
    # Get image and crop by face
    img = camera.takePicture()
    faces = recognizeFace(img)
    if len(faces) == 0:
        response['hasFace'] = False
        return response
    else:
        response['hasFace'] = True
    
    img = cropImage(faces, img)
    img = camera.toImage(img)


    response = edge_classification(img=img)


    return response



